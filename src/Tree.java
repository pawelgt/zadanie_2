import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by pawel on 01.12.2015.
 */
public class Tree<T extends Comparable> {
    private Node<T> root;
    private String DFS_res;

    public Tree(Node<T> root) {
        this.root = root;
    }

    private int compare(Node<T> node1, Node<T> node2) {
//        System.out.println(node1.getValue() + "  " + node2.getValue() + "  = " + node1.compare(node2.getValue()));
        return node1.compare(node2.getValue());
    }

    public void addNode(Node<T> node) {
        insertNode(root, node);
    }

    private Node<T> insertNode(Node<T> parent, Node<T> newNode) {
        if(parent == null) {
            return  newNode;
        }
        else if(compare(parent, newNode) <= -1) {    //parent < node
            parent.setRight(insertNode(parent.getRight(), newNode));
        }
        else if(compare(parent, newNode) >= 1) {    //parent > node
            parent.setLeft(insertNode(parent.getLeft(), newNode));
        }
        return parent;
    }

    public String printDFS() {
        DFS_res = "";
        preorder(root);
        return DFS_res;
    }

    private void preorder(Node node) {
        if(node == null) return ;
        DFS_res += node.getValue() + " ";

        preorder(node.getLeft());
        preorder(node.getRight());
    }

    public String printBFS() {
        String res = "";
        Queue<Node<T>> queue = new LinkedList<>();

        queue.add(root);
        while(!queue.isEmpty()){
            Node node = queue.remove();
            res += node.getValue() + " ";
            if(node.getLeft() != null) {
                queue.add(node.getLeft());
            }
            if(node.getRight() != null) {
                queue.add(node.getRight());
            }
        }

        return res;
    }

    public Node findChild(Node<T> node) {
        Queue<Node<T>> queue = new LinkedList<>();

        queue.add(root);
        while(!queue.isEmpty()){
            Node tmp = queue.remove();
            if(tmp == node || tmp.getValue() == node.getValue()) {
                return tmp;
            }
            if(tmp.getLeft() != null) {
                queue.add(tmp.getLeft());
            }
            if(tmp.getRight() != null) {
                queue.add(tmp.getRight());
            }
        }
        return null;
    }

    public void delete(Node<T> node) {
        delete(root, node);
    }

    private Node delete(Node<T> root_node, Node<T> node) {
        if(root_node == null) {
            return root_node;
        }
        if(compare(node, root_node) == -1) {
            root_node.setLeft(delete(root_node.getLeft(), node));
        }
        else if(compare(node, root_node) == 1) {
            root_node.setRight(delete(root_node.getRight(), node));
        }
        else {
            if(root_node.getLeft() == null) {
                return root_node.getRight();
            }
            else if(root_node.getRight() == null) {
                return root_node.getLeft();
            }

            root_node.setValue(minValue(root_node.getRight()));
            root_node.setRight(delete(root_node.getRight(), root_node));
        }
        return root_node;
    }

    private T minValue(Node<T> root)
    {
        T minv = root.getValue();
        while(root.getLeft() != null)
        {
            minv = root.getLeft().getValue();
            root = root.getLeft();
        }
        return minv;
    }
}
